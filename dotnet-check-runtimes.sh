#!/usr/bin/env bash

declare -a Runtimes=(
    "Microsoft.AspNetCore.App 2.1"
    "Microsoft.AspNetCore.App 3.1"
    "Microsoft.AspNetCore.App 5.0"
    "Microsoft.AspNetCore.App 6.0"
    )

error=0

for runtime in "${Runtimes[@]}";
do
    if [[ ! $( dotnet --list-runtimes | grep "$runtime" ) ]];
    then
        echo `perl -CO -E 'say chr 0x274c'` "Runtime $runtime missing" >&2
        error=$(( $error + 1 ))
    else
        echo `perl -CO -E 'say chr 0x2705'` "Runtime $runtime found"
    fi
done

exit $error