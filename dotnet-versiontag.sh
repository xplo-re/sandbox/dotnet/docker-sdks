#!/usr/bin/env bash

# Parses 'dotnet --list-sdks' and 'dotnet --list-runtimes' and outputs a single
# version identifier string that starts with the most recent SDK, followed by
# each ASP.NET runtime version concatenated by a dash, from newest to oldest.
# Dashes in versions (e.g. pre-releases) are replaced by an underscore.

set -o pipefail

# Remove newline at end of ordered versions list.
echo -n $(
    dotnet --list-sdks |
    # Extract version column.
    cut -s -d " " -f 1 |
    # Sort in reverse order.
    sort --version-sort --reverse |
    # Only keep newest SDK.
    head -n 1
    # Add a single space between both lists.
    ) $(
    dotnet --list-runtimes |
    # Only keep ASP.NET app runtimes.
    grep "Microsoft.AspNetCore.App" |
    # Extract version column.
    cut -s -d " " -f 2 |
    # Sort in reverse order, newest runtime is always listed first.
    sort --version-sort --reverse
    ) |
    # Preview versions have additional version information, separated by a dash.
    # Replace by underscore and concatenate versions with a dash.
    # Docker tags only allow alphanumeric characters, dashes, dots and underscores.
    tr "-" "_" |
    tr " " "-"

# Add final newline.
echo